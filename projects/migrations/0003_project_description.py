# Generated by Django 4.1.7 on 2023-03-06 21:13

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("projects", "0002_rename_projects_project"),
    ]

    operations = [
        migrations.AddField(
            model_name="project",
            name="description",
            field=models.TextField(default=1),
            preserve_default=False,
        ),
    ]
